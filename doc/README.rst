Este directorio contiene el anteproyecto y la memoria del Trabajo Fin de Grado «Middleware de comunicaciones para la composición automática de servicios en IoT».

Para la compilación de dichos documentos es necesario tener instalada la clase esi-tfg.

esi-tfg es una clase LaTeX para escribir comodamente la memoria del TFG (Trabajo Fin de
Grado). 

La normativa actual de la ESI de CR se puede encontrar en:

* http://webpub.esi.uclm.es/archivos/216/NormativaTFG
* http://webpub.esi.uclm.es/archivos/336/Normativa-TFGs

Esta clase LaTeX asume que dispones del paquete texlive_, que es el que se distribuye por
defecto en la mayoría de las distribuciones GNU/Linux, y que también está disponible para
Windows y Mac OSX.

Puedes encontrar el procedimiento de instalación en el siguiente enlace:
 
* http://bitbucket.org/arco_group/esi-tfg/

