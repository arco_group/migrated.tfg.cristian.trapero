\chapter{Configuración de dispositivos del sistema de control de acceso inteligente}
\label{anexo:2}

Para el correcto funcionamiento del sistema de control de acceso inteligente es necesario configurar los parámetros de los dispositivos adecuadamente. A lo largo de este Anexo se ilustrará el proceso de configuración de cada uno de ellos.

\section{Cámara de vigilancia}
Primeramente es necesario conectar la cámara de vigilancia a un punto de acceso WiFi. Para ello es necesario disponer de un smartphone con la aplicación Foscam\footnote{\url{https://play.google.com/store/apps/details?id=com.foscam.foscam&hl=es}} y seguir todo el proceso de instalación de la cámara.

Una vez conectada la cámara al punto de acceso WiFi, es necesario acceder al panel web de la misma. Para ello simplemente es necesario teclear la dirección IP de la cámara, seguida del puerto 88 (ej: 192.168.1.4:88) en el navegador web \textbf{Internet Explorer}, ya que es el único compatible con el servidor web. Seguidamente se instalará una aplicación que contiene los plugins necesarios para su configuración y a continuación, ya se podrá acceder al panel web como se ilustra en la Figura \ref{fig:configuracion-camara0}.

\begin{figure}[h]
	\begin{center}
		\includegraphics[width=0.90\textwidth]{figures/configuracion-camara0.png}
		\caption{Panel web de la cámara Foscam C1}
		\label{fig:configuracion-camara0}
	\end{center}
\end{figure}

En el apartado de ajustes, existen distintas pestañas de configuración. Primeramente se seleccionarán los ajustes FTP para indicarle a la cámara la dirección del servidor donde se enviarán las imágenes capturadas. En la Figura \ref{fig:configuracion-camara1} se especifica la dirección IP del servidor FTP, que en este caso se correspondería con la dirección IP de la Raspberry Pi (\textit{ftp://161.67.106.17}), el puerto del servidor, el modo de conexión y el usuario y la contraseña asociada al servidor FTP.

Seguidamente, es necesario configurar los parámetros del sensor PIR para realizar una captura cuando se detecte movimiento delante de la misma. Para ello, en la Figura \ref{fig:configuracion-camara2} se ilustra los parámetros asociados a cada variable de la configuración. 

Por ultimo, para que la cámara envíe las imágenes capturadas al servidor FTP cuando el sensor PIR detecte movimiento, es necesario configurar los parámetros en la pestaña \textit{Snapshot Settings} como ilustra la Figura \ref{fig:configuracion-camara3}.


\begin{figure}[h]
	\begin{center}
		\includegraphics[width=0.90\textwidth]{figures/configuracion-camara1.png}
		\caption{Ajustes FTP de la cámara de vigilancia}
		\label{fig:configuracion-camara1}
	\end{center}
\end{figure}

\begin{figure}[h]
	\begin{center}
		\includegraphics[width=0.90\textwidth]{figures/configuracion-camara2.png}
		\caption{Ajustes del sensor PIR}
		\label{fig:configuracion-camara2}
	\end{center}
\end{figure}

\begin{figure}[h]
	\begin{center}
		\includegraphics[width=0.99\textwidth]{figures/configuracion-camara3.png}
		\caption{Ajustes de la captura de imágenes}
		\label{fig:configuracion-camara3}
	\end{center}
\end{figure}

Con todos estos pasos ya estará configurada la cámara de vigilancia IP para enviar una imagen, cuando se detecte movimiento, a la Raspberry Pi a través del servidor FTP.

\section{Raspberry Pi}
La Raspberry Pi es el <<\textit{centro neurálgico}>> del sistema, puesto que todos los servicios se ejecutan en ella. Para el correcto funcionamiento de los servicios, fue necesario instalar el servidor FTP, configurar la interfaz de audio USB para poder grabar la voz de los usuarios con el micrófono e instalar todas los paquetes o dependencias de los servicios del sistema.

El sistema operativo instalado en la Raspberry Pi es la distribución Raspbian. Raspbian es un sistema operativo gratuito basado en Debian optimizado para el hardware Raspberry Pi.

\subsection{Instalación del servidor FTP}
Todo el proceso de instalación del servidor FTP aquí descrito está basado en el tutorial de Geeky Theory: \url{https://geekytheory.com/tutorial-raspberry-pi-9-servidor-ftp} 

\begin{enumerate}
	\item Descargar el servidor vsftpd:
	\begin{console}
	sudo apt-get install vsftpd
	\end{console}
	
	\item Una vez que este descargado abrimos el siguiente archivo de configuración: 
	\begin{console}
	sudo nano /etc/vsftpd.conf
	\end{console}
	
	Y descomentamos las siguientes líneas para permitir la escritura de archivos a los usuarios de la Raspberry Pi.
	\subitem local\_enable=YES
	\subitem write\_enable=YES
	\item Por último reiniciamos el servicio.
	\begin{console}
	sudo service vsftpd restart
	\end{console}	
\end{enumerate}

Con todos estos pasos ya se podría realizar una conexión FTP utilizando el usuario por defecto de Raspbian, denominado pi, con su contraseña asociada.

\subsection{Configuración de tarjeta de sonido USB}
La Raspberry Pi dispone de un conector minijack de audio incorporado en su placa base, pero no permite la conexión de micrófono, por lo cual, fue necesaria la utilización de la tarjeta de audio USB Logilink UA0053. 

El sistema operativo Raspbian utiliza por defecto la tarjeta integrada de la Raspberry Pi, por lo cual es necesario cambiar los parámetros correspondientes para seleccionar la tarjeta USB como principal. Para ello, únicamente es necesario ejecutar la siguiente instrucción:
\begin{console}
	sudo nano /etc/asound.conf 
\end{console}
Y añadir las siguientes lineas:
\begin{console}
pcm.!default  {
	type hw card 1
}
ctl.!default {
	type hw card 1
}
\end{console}
Por ultimo, es necesario reiniciar el dispositivo para que los cambios tengan efecto:
\begin{console}
sudo reboot
\end{console}

Con todo esto, ya estará seleccionada la tarjeta USB como tarjeta de audio principal. Para comprobar el funcionamiento de la misma, únicamente es necesario conectar el micrófono de solapa a la entrada de micrófono de la tarjeta de audio y ejecutar la siguiente instrucción para empezar a grabar audio:

\begin{console}
	arecord --device=hw:1,0 --format S16_LE --rate 44100 -c1 test.wav
\end{console}

Si se escucha el sonido proveniente del micrófono, todo funciona correctamente.

\section{Sonoff}
Para poder programar el chip ESP82266EX integrado en el Sonoff Basic es necesario que la interfaz de programación serie del módulo esté disponible. Para ello, únicamente es necesario abrir el Sonoff quitando la carcasa que lo recubre y ya estará accesible el pinout del dispositivo tal y como se muestra en la Figura \ref{fig:serial-pinout}.

\begin{figure}[h]
	\begin{center}
		\includegraphics[width=0.70\textwidth]{figures/sonoff-serial-pinout.jpg}
		\caption{Serial pinout del Sonoff}
		\label{fig:serial-pinout}
	\end{center}
\end{figure}

Uno de los requisitos para poder programar el chip es disponer de un convertidor o programador FTDI TTL de 3.3V como el de la Figura \ref{fig:ftdi}.

\begin{figure}[h]
	\begin{center}
		\includegraphics[width=0.30\textwidth]{figures/ftdi.jpg}
		\caption{FT232R - USB UART IC}
		\label{fig:ftdi}
	\end{center}
\end{figure}


Para programar el dispositivo, es necesario soldar una tira de 4 pines en el pinout del Sonoff. Seguidamente, para entrar en el modo programación, es necesario \textbf{conectar el FTDI} a esta tira de pines como se especifica en el Cuadro \ref{conexion-ftdi}. Después, pulsar el switch que incorpora el Sonoff y sin soltar el switch conectar la alimentación del FTDI de 3.3V. Una vez haya arrancado el Sonoff se puede soltar el botón. 

\begin{table}[]
	\centering
	\begin{tabular}{|c|c|}
		\hline
		\rowcolor[HTML]{C0C0C0} 
		\textbf{FTDI} & \textbf{Sonoff} \\ \hline
		3V3           & 3V3 / VCC       \\ \hline
		TX            & RX              \\ \hline
		RX            & TX              \\ \hline
		GND           & GND             \\ \hline
	\end{tabular}
	\label{conexion-ftdi}
	\caption{Conexiones entre los pines del FTDI y el Sonoff}
\end{table}

A continuación, se cargará el firmware a través del editor Atom y PlatformIO\footnote{\url{http://platformio.org/}}.

Platform IO es un IDE para el desarrollo integrado de aplicaciones en dispositivos IoT. Dispone de un sistema de compilación multiplataforma sin dependencias externas al sistema operativo. Gracias a su integración con el editor de texto Atom, se puede realizar la carga de firmware con dos simples pasos. 

Lo primero de todo es instalar PlatformIO en el editor Atom. Para ello, unicamente es necesario seguir el tutorial descrito en este enlace:\url{http://platformio.org/get-started/ide?install=atom}. 

Seguidamente es necesario \textbf{instalar las librerías} de IceC para la compilación del firmware. Para ello, nos dirigimos a la pestaña \texttt{Libraries} del panel de Platform IO e instalamos la librería \textbf{IceC}\footnote{\url{http://platformio.org/lib/show/1673/IceC}} e \textbf{IceC-IoT-node} tal y como se ilustra en la Figura \ref{fig:platformio1} y \ref{fig:platformio2}.


\begin{figure}[h]
	\begin{center}
		\includegraphics[width=0.90\textwidth]{figures/platformio1.png}
		\caption{Instalación de la librería IceC}
		\label{fig:platformio1}
	\end{center}
\end{figure}

\begin{figure}[h]
	\begin{center}
		\includegraphics[width=0.90\textwidth]{figures/platformio2.png}
		\caption{Instalación de la libreria IceC-IoT-node}
		\label{fig:platformio2}
	\end{center}
\end{figure}

\begin{figure}[h]
	\begin{center}
		\includegraphics[width=0.90\textwidth]{figures/platformio3.png}
		\caption{Abrir el proyecto en Platform IO}
		\label{fig:platformio3}
	\end{center}
\end{figure}

A continuación, compilaremos el código fuente para generar el firmware que será flasheado en el Sonoff. Para ello, primeramente cargaremos el proyecto en la plataforma con la opción \texttt{Open Project} y seleccionando la carpeta que contiene el código fuente del dispositivo (ver Figura \ref{fig:platformio3}).

Una vez cargado el proyecto, abrimos el archivo \texttt{platformio.ini} y lo compilamos con la opción \texttt{Build} del panel superior PlatformIO o con la combinación de teclas Alt + Control + B. Y por ultimo, con el FTDI conectado al ordenador y este a su vez al Sonoff, seleccionamos la opción \texttt{Upload} o combinación de teclas Alt + Control + U. 

Con todos estos pasos se ha conseguido compilar y flashear el servicio en el Sonoff que controlará la apertura del abrepuertas.
