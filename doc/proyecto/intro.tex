\chapter{Introducción}
\section{Motivación}
\label{chap:introduccion}

\drop{A} {día} de hoy el mundo físico está ricamente entretejido con sensores, actuadores, visualizadores y elementos computacionales integrados a la perfección en los objetos cotidianos de nuestras vidas, haciendo una realidad paradigmas como la computación ubicua o el \textit{Internet de las Cosas} (\ac{IoT} por sus siglas en inglés). La inclusión de estos paradigmas, tanto en entornos domésticos como laborales, ha propiciado la aparición de edificios inteligentes o \textit{Smart Buildings}, los cuales nos permiten una gestión integrada y automatizada de los mismos con el fin de aumentar la eficiencia energética, la seguridad, la usabilidad y la accesibilidad entre otros \cite{SmartBuildings}. 

Uno de los sectores emergentes en el marco de la sostenibilidad medioambiental, social y económica son las ciudades inteligentes o \textit{Smart Cities}. Con las Smart Cities se quieren solventar problemas como el abastecimiento energético o la contaminación, utilizando para ello, las tecnologías de la información y paradigmas como los mencionados anteriormente. 

Las soluciones tecnológicas disponibles para el despliegue de este tipo de entornos están orientadas a la utilización de tecnologías propietarias en las cuales la interoperabilidad entre los sistemas se ve limitada por el uso de software y hardware \textit{ad-hoc}. Este tipo de soluciones comúnmente utilizan infraestructuras de computación en la nube, con la cuales se facilitan la integración de un numero muy elevado de dispositivos, proporcionando una manera simplificada de gestión de datos. Pero por otra parte, la centralización de las aplicaciones, la madurez funcional de las mismas y la disponibilidad de los servicios, origina una interdependencia con los proveedores de servicios que limita la interoperabilidad entre tecnologías.

Algunas de las soluciones que se han propuesto para resolver esta problemática, vienen desde el intento de estandarización de protocolos de comunicación como ZigBee, 6LowPan o \ac{MQTT}; desarrollo de middlewares de comunicaciones como OpenIoT\footnote{\url{http://www.openiot.eu/}}, OpenRemote\footnote{\url{http://www.openremote.com/}} o Kaa\footnote{\url{https://www.kaaproject.org/}}; o la fundación de organizaciones reguladoras como OneM2M, IPSO Alliance o AllSeen Alliance.

Las tecnologías middleware han permitido abstraer la complejidad y la heterogeneidad de las redes de comunicaciones subyacentes a los sistemas distribuidos, pudiendo interconectar y comunicar los elementos que la conforman. Sin embargo, uno de los retos mas destacables del \ac{IoT} pasa por dar soporte a la interoperabilidad entre servicios y por contextualizar semánticamente los datos generados por los mismos, de forma que se pueda inferir conocimiento que aporte valor. 

Minsky \cite{Minsky} considera que la ausencia de sentido común es la responsable de las deficiencias de los sistemas computacionales actuales. La carencia de conocimiento de sentido común, la orientación de los sistemas a objetivos específicos y la ausencia de procesos de razonamiento e inferencia, provoca que dichos sistemas se limiten a generar una respuesta elaborada para un fin concreto \cite{7794099}. 

Paradigmas como la Inteligencia Ambiental (\ac{AmI}) son un claro ejemplo de la necesidad existente de contextualizar la información generada por los dispositivos electrónicos integrados en un entorno, de forma que se habiliten procesos de adaptación al contexto que permitan prever, identificar y satisfacer las necesidades emergentes en un entorno inteligente. Para ello es necesario emplear mecanismos que articulen la generación de respuestas de forma autónoma.

La Unión Europea, a través de sus convocatorias de financiación H2020 para IoT (H2020-IOT-2016-2017)\footnote{\url{https://goo.gl/XK4aNf}}, ha identificado como reto específico la necesidad de ir más allá de las meras plataformas de sensorización poniendo el foco en las capacidades de actuación y comportamiento inteligente. Con este objetivo, este \ac{TFG} pretende investigar y avanzar en el desarrollo de una plataforma \ac{IoT} con capacidad de comprensión y actuación no supervisada, respuesta ante situaciones imprevistas, integración del factor humano (Human in the Loop Cyber-Physical Systems) y mejora de la interoperabilidad y robustez ante fallos en sistemas distribuidos y heterogéneos.

En el desarrollo de este proyecto se aunarán los esfuerzos necesarios para implementar un middleware de comunicaciones semántico que permita dar soporte al entendimiento del contexto y a la elaboración automática de respuestas ante situaciones imprevistas, basadas en la reconfiguración y composición de los servicios disponibles en un entorno inteligente.

La arquitectura propuesta estará basada en el middleware orientado a objetos Ice de la empresa ZeroC\footnote{\href{https://zeroc.com/}{https://zeroc.com/}}, el cual proporciona todos los mecanismos necesarios para dar soporte a la comunicación entre los servicios de una aplicación distribuida. Para el tratamiento uniforme de los servicios, Ice nos proporciona un lenguaje de especificación de interfaces denominado \textit{Slice}, con el que definir un mecanismo de abstracción que nos permita el desarrollo de servicios interoperables entre sí.

Para sustentar el mecanismo de composición de servicios es necesario dar respuesta al problema de la representación semática de las funcionalidades que proveen los servicios y la forma que tienen estos de interactuar con el entorno. La solución empleada para resolver dicha problemática, pasa por utilizar un sistema de representación de conocimiento como \textit{Scone}. Scone \cite{Fahlman2006} es una base de conocimiento (\ac{KB}) de código abierto de alto rendimiento diseñada para ser utilizada en aplicaciones software. Este sistema provee soporte a la representación de conocimiento simbólico sobre el mundo, como pueda ser conocimiento de sentido común o de un dominio de aplicación específico.

Para ilustrar el funcionamiento del middleware, se han desplegado una serie de dispositivos y servicios en un entorno laboral, que permitirán al sistema realizar una reconfiguración y composición de los mismos de forma que se proporcione un servicio de control de acceso inteligente basado en el reconocimiento facial y de voz de los usuarios.


\section{Estructura del documento}

Esta sección pretende dar al lector una idea general del contenido del presente documento, explicando brevemente el contenido de las diferentes secciones del mismo, de modo que pueda localizar de forma sencilla la información que sea de su interés.

\begin{definitionlist}
	\item[Capítulo \ref{chap:objetivos}: \nameref{chap:objetivos}] Finalidad y justificación del presente documento.
	
	\item[Capítulo \ref{chap:antecedentes}: \nameref{chap:antecedentes}] Contextualiza el estado del arte de los paradigmas y las diferentes tecnologías que han sido empleadas para el desarrollo de este proyecto.
	
	\item[Capítulo \ref{chap:metodologia}: \nameref{chap:metodologia}] Se indica el procedimiento y el conjunto de herramientas empleadas para el desarrollo del proyecto.
	
	\item[Capítulo \ref{chap:desarrollo}: \nameref{chap:desarrollo}] Se contextualiza la problemática que se quiere resolver y el proceso de desarrollo del proyecto.
	
	\item[Capítulo \ref{chap:resultados}: \nameref{chap:resultados}] Se recogen los resultados del proyecto obtenidos de la aplicación del método de trabajo.
	
	\item[Capítulo \ref{chap:conclusiones}: \nameref{chap:conclusiones}] En este capítulo se valora el trabajo realizado, los objetivos que se han conseguido y se indican algunas posibilidades de trabajo futuro sobre este proyecto.
	
\end{definitionlist}
