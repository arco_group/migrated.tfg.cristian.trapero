\chapter{Método de trabajo}
\label{chap:metodologia}

\drop{E}{n} este capitulo se describe la metodología de trabajo que se ha utilizado para el desarrollo del proyecto, abordando las nociones teóricas que conciernen a la metodología elegida y cómo se ha llevado a cabo su aplicación. Seguidamente presentaremos las herramientas tanto hardware como software que han sido empleadas para el desarrollo del proyecto.


\section{Metodología}
Para poder llevar a cabo el proyecto se han analizado diferentes metodologías tradicionales y metodologías ágiles. Las metodologías tradicionales imponen una disciplina de trabajo sobre el proceso de desarrollo del software, con el fin de conseguir un software más eficiente. Se centran especialmente en el control del proceso, mediante una rigurosa definición de roles, actividades, artefactos, herramientas y notaciones para el modelado y documentación detallada. 
Este tipo de metodologías se adecuan a proyectos en los que se parte de una especificación precisa de los requisitos, donde los objetivos del proyecto se han definido claramente. 

Por el contrario, las metodologías ágiles abogan por la planificación adaptativa, el desarrollo evolutivo, la entrega temprana y la mejora continua, de forma que se fomenta una respuesta rápida y flexible al cambio. El Manifiesto por el Desarrollo Ágil de Software\footnote{\href{http://agilemanifesto.org/iso/es/principles.html}{http://agilemanifesto.org/iso/es/principles.html}} contrasta las virtudes de las metodologías ágiles frente a las metodologías tradicionales de la siguiente forma:

\begin{itemize}
	\item \textbf{Individuos e interacciones} sobre procesos y herramientas
	
	\item \textbf{Software funcionando} sobre documentación extensiva
	
	\item \textbf{Colaboración con el cliente} sobre negociación contractual

	\item \textbf{Respuesta ante el cambio} sobre seguir un plan
\end{itemize}

Uno de los doce principios del Manifiesto ágil describe que:

\textit{<<Aceptamos que los requisitos cambien, incluso en etapas 
tardías del desarrollo. Los procesos Ágiles aprovechan
el cambio para proporcionar ventaja competitiva al 
cliente.>>}

Con lo que respecta a los objetivos marcados en el planteamiento de este proyecto, se abordan problemas referentes al diseño, desarrollo y despliegue tanto de sistemas hardware como software para lo cuales los requisitos no están claramente definidos. Es por ello que este tipo de metodologías se adaptan perfectamente a la naturaleza del proyecto.

Algunas de las metodologías ágiles mas utilizadas son: 

\begin{itemize}
\item \textbf{Scrum}: Adopta una estrategia de desarrollo incremental, en lugar de la planificación y ejecución completa del producto. La calidad del resultado se mide por el conocimiento tácito de las personas en equipos auto-organizados, más que por la calidad de los procesos empleados. Y además existe un solapamiento de las fases de desarrollo, en lugar de realizar una tras otra en un ciclo secuencial o en cascada.

\item \textbf{Programación Extrema (XP)}: Está centrada en potenciar las relaciones interpersonales como clave para el éxito en desarrollo de software, promoviendo el trabajo en equipo, preocupándose por el aprendizaje de los desarrolladores, y propiciando un buen clima de trabajo.

\item \textbf{Kanban}: Kanban es un símbolo visual que se utiliza para desencadenar una acción. A menudo se representa en un tablero Kanban para reflejar los procesos de su flujo de trabajo. Requiere una comunicación en tiempo real sobre la capacidad y una transparencia del trabajo total.
\end{itemize}

Inicialmente este proyecto tenía una orientación estrictamente académica, pero las posibilidades de integración que ofrecía, permitieron enmarcar el desarrollo del mismo dentro de un proyecto europeo, como es CitiSim.

Puesto que los objetivos del proyecto no estaban bien enmarcados y las necesidades del cliente, en este caso el proyecto europeo CitiSim, no establecía un criterio definido para la integración del sistema, fue necesario recurrir al modelo de prototipos para la construcción de prototipos rápidos con los cuales articular una retroalimentación por parte del cliente para la nueva definición de requisitos. 

\section{Prototipado}
El propósito de un prototipo es permitir que los usuarios del software evalúen las propuestas de los desarrolladores para el diseño del producto eventual al probarlas, en lugar de tener que interpretar y evaluar el diseño según las descripciones. Los prototipos también pueden ser utilizados por los usuarios finales para describir y probar requisitos que no se han tenido en cuenta, y que pueden ser un factor clave en la relación comercial entre los desarrolladores y sus clientes \cite{smith1991software}.

El proceso de prototipado involucra cuatro pasos:
\begin{enumerate}
	\item \textbf{Identificar} los requerimientos básicos.
	\item \textbf{Desarrollar} un prototipo inicial.
	\item \textbf{Revisar} el prototipo.
	\item \textbf{Mejorar} el prototipo.
\end{enumerate}

Existen dos formas de abordar el desarrollo de un prototipo, una en la dimensión horizontal y la otra en vertical. La primera de ellas pretende proporcionar una visión amplia de todo un sistema o subsistema, centrándose más en la interacción del usuario que en la funcionalidad del sistema de bajo nivel. En cambio, la dimensión vertical permite detallar en mas profundidad una función del sistema en base a los requerimientos.

Existen distintas variantes de prototipado: desechable, evolutivo,  incremental y extremo. En este caso hemos optado por basar el desarrollo del proyecto en una metodología de prototipado evolutivo.

\subsection{Prototipado evolutivo}
El objetivo principal al usar prototipos evolutivos es construir un prototipo muy robusto de una manera estructurada y refinarlo constantemente. La razón de este enfoque es que el prototipo evolutivo, cuando se construye, forma el corazón del nuevo sistema, y luego se construirán las mejoras y los requisitos adicionales.

Este modelo se basa en el desarrollo rápido de prototipos de forma que pueda obtenerse
una retroalimentación por parte del cliente en poco tiempo. La retroalimentación ayuda al desarrollador a refinar los requisitos del software a desarrollar y a entender mejor qué debe hacerse. También permite ir agregando funcionalidades a lo largo de las iteraciones, por lo que en cada iteración el prototipo irá evolucionando hacia una forma más completa de sí mismo.

El prototipado evolutivo rompe el orden lineal de las etapas de desarrollo de las metodologías tradicionales y las mapea como ciclos de desarrollo sucesivos. Dependiendo del grado de ciclos, podemos distinguir dos formas de desarrollo \cite{Floyd1984}:

\begin{enumerate}
	\item \textbf{Desarrollo de sistemas incremental}: Se fundamentan en la idea de que los problemas complejos pueden ser descompuestos en problemas mas simples, que pueden ser resueltos de forma gradual hasta llegar a la solución final. Es considerada una estrategia de desarrollo a largo plazo, y el diseño de los aspectos técnicos se realiza en un proceso de aprendizaje y crecimiento progresivo donde los usuarios pueden participar, obteniendo con ello beneficios obvios en lo relativo a la comunicación entre usuarios y desarrolladores.
	
	\item \textbf{Desarrollo de sistemas evolutivo}: Trabaja con secuencias de ciclos formados por un (re-)diseño, una (re-)implementación y una (re-)evaluación, dependiendo del número de ciclos a realizar de cada proyecto concreto. Este prototipado es utilizado en entornos dinámicos y cambiantes, llevando a dejar de lado la previa recogida del conjunto completo de requisitos con el fin de adaptarse a cambios posteriores, impredecibles.
\end{enumerate}

\begin{figure}[h]
	\begin{center}
		\includegraphics[width=0.45\textwidth]{figures/prototipado-evolutivo.png}
		\caption[Fases del prototipado evolutivo]{Fases del prototipado evolutivo\protect\footnotemark}
		\label{fig:prototipado-evolutivo}
	\end{center}
\end{figure}

\footnotetext{Imagen diseñada por Ana Rubio Ruiz}

El prototipado evolutivo empieza con la recogida y definición de los requisitos del sistema, que permiten diseñar el primer prototipo básico que cumpla en parte con los objetivos marcados por el cliente. Una vez se ha diseñado el prototipo se procede a su construcción, un desarrollo rápido que permita presentar el modelo en un corto plazo de tiempo al cliente, de forma que pueda ser evaluado a posteriori.

En la fase de evaluación se pondrá en contexto el prototipo construido con la finalidad de redefinir y añadir nuevos requerimientos al sistema en la fase de refinamiento del producto. Dicha fase permitirá el rediseño del prototipo, de forma que una nueva versión podrá ser construida y presentada de nuevo al cliente, repitiendo el proceso hasta que el producto final se ajuste a sus necesidades. La figura \ref{fig:prototipado-evolutivo} ilustra gráficamente el proceso.

Para el desarrollo de este trabajo se ha optado por utilizar la metodología de prototipado evolutivo por ser la que mejor se adapta a las necesidades del proyecto. 

\subsection{Herramientas}
	
En esta sección se presentan todas las herramientas que han sido utilizadas a lo largo de todo el proyecto, tanto hardware como software y una pequeña descripción de su propósito.

\subsubsection{Hardware}
Las herramientas hardware utilizadas en el proceso de desarrollo del proyecto son las siguientes:

\begin{itemize}
	\item \textbf{Ordenador de sobremesa}: Procesador Intel Core i5-760, 8 GB RAM, 500 GG HDD.
	
	\item \textbf{Ordenador portatil}: \textit{Mountain StudioMX 15 Ivy}: Procesador Intel Core i7 (3º Gen), 24 GB RAM, 128 GB SSD, 700 GB HDD, Nvidia GTX 660M 2 GB.
	\item \textbf{August Doorbell Cam (v.1)}: Timbre con cámara integrada, utilizado para el despliegue del control de acceso August.
	
	\item \textbf{August Smart Keypad}: Teclado numérico utilizado para el despliegue del control de acceso August.
	
	\item \textbf{August Smart Lock Pro (v.2)}: Cerradura electrónica utilizada para el despliegue del control de acceso August.
	
	\item \textbf{Cámara Foscam C1}: Cámara de vigilancia IP utilizada para el control de acceso inteligente. 
	
	\item \textbf{Raspberry Pi 2}: Computador de placa reducida de bajo costo utilizada para la integración del micrófono de solapa en el control de acceso inteligente.
	\item \textbf{Micrófono de solapa}: Utilizado para la grabación de voz en el control de acceso inteligente.
	
	\item \textbf{LogiLink USB Soundcard}: Interfaz de audio USB para la conexión del microfono de solapa a la Raspberry Pi 2.
	
	\item \textbf{Linksys WRT54G}: Router inalámbrico utilizado para el despliegue de una red con la que interconectar los dispositivos.
	
	\item \textbf{Dongle USB WiFi}: Interfaz de red WiFi utilizada para la interconexión de la Raspberry Pi al router inalámbrico.
	
	\item \textbf{Sonoff WiFi Basic}: Interruptor de encendido de control remoto por WiFi, utilizado para controlar la apertura de la cerradura eléctrica de la puerta en la que está instalado el sistema de control de acceso inteligente.
	
	\item \textbf{Driver de LED}: Transformador de tensión de corriente alterna a corriente continua, para el control del abrepuertas eléctrico.
	
	\item \textbf{Abrepuertas eléctrico}: Requerido para el control de la apertura de la puerta.
	
	\item \textbf{FTDI}: Utilizado para el flasheo del firmware del Sonoff Basic.
\end{itemize}

\subsubsection{Software}
Las herramientas software utilizadas en el proceso de desarrollo del proyecto son las siguientes:
\paragraph{Sistemas operativos}
\begin{itemize}
	\item \textbf{Debian 9.0 Stretch}: Debian es un sistema operativo libre tipo Unix que está compuesto completamente de software libre y empaquetado por un grupo de personas que participan en el Proyecto Debian.
	
	\item \textbf{Raspbian Stretch}: Raspbian es una distribución del sistema operativo GNU/Linux basado en Debian Stretch (Debian 9.0) para la placa computadora (SBC) Raspberry Pi.
\end{itemize}

\paragraph{Lenguajes de programación}
El lenguaje de programación principal utilizado en el desarrollo del proyecto ha sido \textbf{Python}, tanto en su versión 2.7 como 3.6. Python es un lenguaje de programación multiparadigma, interpretado y multiplataforma.

Para la definición del conocimiento de la base de conocimiento Scone, se ha utilizado el lenguaje de programación \textbf{Steel Bank Common Lisp}, así como para las pruebas unitarias de dicha base de conocimiento.

\paragraph{Aplicaciones}
\begin{itemize}
	\item \textbf{\href{https://atom.io/}{Atom}}: Es un editor de código de fuente de código abierto para macOS, Linux, y Windows con soporte para plugins escritos en Node.js y control de versiones Git integrado, desarrollado por GitHub.
	
	\item \textbf{\href{https://www.latex-project.org/}{\LaTeX}}: Sistema de composición de texto de alta calidad que incluye características diseñadas para la producción de documentación técnica y científica. LaTeX  es el estándar de facto para la comunicación y publicación de documentos científicos.
	
	\item \textbf{\href{https://www.texstudio.org/}{TeXstudio}}: Entorno de escritura integrado para crear documentos \LaTeX. TeXstudio es de código abierto y está disponible para todos los principales sistemas operativos.
	
	\item \textbf{\href{https://www.mercurial-scm.org/}{Mercurial}}: Sistema de control de versiones multiplataforma, para desarrolladores de software. Ofrece la capacidad de manejar de manera eficiente proyectos de cualquier tamaño mientras utiliza una interfaz intuitiva. 
	
	\item \textbf{\href{https://www.mendeley.com}{Mendeley}}: Aplicación web y de escritorio, propietaria y gratuita, que permite gestionar y compartir referencias bibliográficas y documentos de investigación, encontrar nuevas referencias y documentos y colaborar en línea.
	
	\item \textbf{\href{https://inkscape.org}{Inkscape}}: Software de vectores gráficos de calidad profesional para Windows, Mac OS X y GNU/Linux. Es usado por diseñadores profesionales y aficionados de todo el mundo para crear una gran variedad de gráficos como ilustraciones, iconos, logos, diagramas, mapas y diseños web.
	
	\item \textbf{\href{https://www.meistertask.com/es}{Meistertask}}: Herramienta de gestión de tareas en formato visual inspirado en los tableros Kanban. Permite añadir tantos miembros de equipo cómo necesites, para poder trabajar de forma colaborativa y en tiempo real. 
	
	\item \textbf{\href{https://bitbucket.org/}{Bitbucket}}: Bitbucket es un servicio de alojamiento basado en web, para los proyectos que utilizan el sistema de control de revisiones Mercurial y Git.
	
	\item \textbf{\href{https://cacoo.com/}{Cacoo}}: Software para la generación de diagramas online que permite el modelado de wireframes, modelos UML y diagrama de red entre otros.
	
	\item \textbf{\href{http://platformio.org/}{Platform IO}}: Plataforma de código abierto que permite el desarrollo de aplicaciones para dispositivos IoT. Integrable con el editor de texto Atom.
	
\end{itemize}
