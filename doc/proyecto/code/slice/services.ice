// -*- mode: c++; coding: utf-8 -*-
// This file contents should be coordinated with CITISIM partners
// acording project requirements


#include "iot.ice"

module SmartObject {

  interface SnapshotService extends EventSink, Observable {
    void trigger(byte count, float deltaSeconds);
  };

  interface ClipService extends EventSink, Observable {
    // Dice Óscar que esto no tiene sentido (i).
    void trigger(byte durationSeconds);
  };

  // Necesitamos aquí estas ifaces si la KB es quien conoce la semántica?
  interface FaceDetector extends DataSink, Observable {};
  interface PersonRecognizer extends DataSink, Observable {};
  interface SpeechToText extends DataSink, Observable {};

  interface AuthenticatedCommandService extends Observable {
    void notifyPerson(string personID, Metadata meta);
    void notifyCommand(string command, Metadata meta);
  };
};


// State: PROPOSAL
module SmartEnvironment {

  struct PersonMovement {
    string personID;
    string areaOfInterest;
    string type; // INBOUND, OUTBOUD
    string timestamp;
  };

  sequence<PersonMovement> PersonMovementSeq;

  interface PersonMovementMonitor {
    void notify(PersonMovementSeq movements);
  };

  struct OccupancyChange {
    string areaOfInterest;
    int amount;
    string timestamp;
  };

  sequence<OccupancyChange> OccupancySeq;

  interface OccupancyMonitor {
    void notify(OccupancySeq occupancies);
  };
};
