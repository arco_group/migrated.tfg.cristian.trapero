// -*- mode: c++; coding: utf-8 -*-

// This file contents should be coordinated with CITISIM partners
// acording to project requirements

module SmartObject {

    // REFACT: linkTo is better name
    interface Observable {
	void setObserver(string observer);
    };

    enum MetadataField {
	Timestamp,    // Unix time
	Quality,      // 0:unknown, 1:poor, 255:best
	Expiration,   // Seconds
	Latitude,     // GPS coordinates (ie. 42.361020)
	Longitude,    // GPS coordinates (ie. 0.160056)
	Altitude,     // GPS coordinates (ie. 1852.25)
	Place,	      // Unique human readable place identifier

	// Intended for batching
	// State: Proposal
	Delta,        // Time between measures
	Size          // Bytes of each measure
    };

    dictionary <MetadataField, string> Metadata;
    sequence<byte> ByteSeq;

    // G-Force sensed on each axis (m/s² or Gs)
    struct Acceleration {
	float x;
	float y;
	float z;
    };

    interface DigitalSink {
	void notify(bool value, string source, Metadata data);
    };

    interface AnalogSink {
	void notify(float value, string source, Metadata data);
    };

    interface DataSink {
        void notify(ByteSeq data, string source, Metadata meta);
    };

    interface EventSink {
	void notify(string source, Metadata data);
    };

    interface AccelerometerSink {
	void notify(Acceleration value, string source, Metadata data);
    };
};
