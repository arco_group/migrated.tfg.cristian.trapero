// -*- mode: c++; coding: utf-8 -*-

#include "iot.ice"

module Citisim {

  // State: PROPOSAL
  module EventService {

    enum Criterion {
      TimestampBegin,
      TimestampEnd,
      Place
      // more...
    };

    dictionary <Criterion, string> Filter;

    interface Channel {
      void destroy();
      void unsubscribe(Object* obj);
      void subscribe(Object* s, Filter f)throws AlreadySubscribed, BadFilter;
      idempotent Object* getPublisher();
    };

    exception AlreadySubscribed {};
    exception BadFilter {};

    interface ChannelManager {
      Channel* getChannel(string name);
    };

    interface EventPersister {
      string replay(string channel, Object* s, Filter q);  // return replay UUID
      void cancel(string uuid);                            // cancel replay session
    };
  };

  // State: PROPOSAL
  module DiscoveryService {
    // 'uuid' is static and non-proxy-related,
    // 'address' is proxy related and may be dynamic
    void adv(string uuid, string address);

  };
};





// sink = adapter.add(AnalogSinkI())
// manager = stringToProxy("event-service");
// channel = manager.getAnalogChannel("temperature");
// channel.subscribe(sink, {})


publicador = ...
persister = stringToProxy('lights-')
persister.replay('luces', publicador, filter)
