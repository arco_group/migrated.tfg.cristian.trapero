\chapter{Resultados}
\label{chap:resultados}

\drop{E}{n} este capítulo se recogen los resultados obtenidos tras la elaboración del proyecto. Para ello, se evaluará el grado de cumplimiento de los objetivos propuestos al inicio del proyecto y se especificará de forma mas detallada la arquitectura del middleware. Por último, se pondrá en contexto todo el proceso de instalación y funcionamiento del sistema de control de acceso inteligente diseñado para el proyecto. 

\section{Arquitectura del middleware}
La arquitectura propuesta en este trabajo ha sido especialmente diseñada para habilitar dos capacidades claves:

\begin{itemize}
	\item La capacidad de comprender automáticamente las necesidades del contexto y los medios de actuación disponibles para satisfacer dichas necesidades.
	\item La capacidad de integrar, reconfigurar y componer los servicios disponibles en un entorno IoT.
\end{itemize}

Para este fin, la arquitectura descrita en la Figura \ref{fig:arquitectura-middleware} organiza en cuatro capas las habilidades que se ocupan de las capacidades mencionadas anteriormente. 

\begin{figure}[h]
	\begin{center}
		\includegraphics[width=0.99\textwidth]{figures/arquitectura-middleware.png}
		\caption{Arquitectura del middleware semántico}
		\label{fig:arquitectura-middleware}
	\end{center}
\end{figure}

En la parte mas baja de la arquitectura se encuentran los \textbf{servicios del entorno IoT}, que en este proyecto se corresponderían con los servicios del sistema de control de acceso inteligente. Para el tratamiento uniforme de dichos servicios, se requiere de una \textbf{capa de abstracción} que permita homogeneizar los protocolos de comunicaciones empleados por los distintos dispositivos y servicios. Para este fin, IDM \cite{Villa2017} propone un enfoque novedoso consistente en un \textbf{protocolo virtual de red} que permite el intercambio de mensajes inter-dominio, direccionando y accediendo a cada uno de los objetos IoT independientemente de la tecnología de comunicaciones empleada. Es conveniente decir que durante el desarrollo de este proyecto, no ha sido necesario emplear dicho protocolo de comunicaciones, puesto que todos los dispositivos empleados en el proyecto utilizan tecnología inalámbrica WiFi. 

Por encima de esta capa, se encuentra el \textbf{middleware orientado a objetos} de propósito general ZeroC Ice, que ha sido provisto de capacidades avanzadas con las que poder extender las funcionalidades del mismo para automatizar distintas tareas como el cableado de los servicios o el filtrado de eventos, entre otros.
Todas los servicios que conforman el middleware se sustentan sobre un conjunto de \textbf{interfaces de programación} bien conocidas, cuya semántica ha sido reflejada en la base de conocimiento Scone. 

En la capa superior de la arquitectura, se dispone de la \textbf{capa de conciencia} o de agentes, que sustenta los modelos de comportamiento que determinan como el sistema ha de reaccionar ante la detección de una necesidad. En lo que respecta a este proyecto, esta tarea se ha simplificado mediante la definición de la necesidad de abrir una puerta (\textit{open door}).

De forma transversal a la arquitectura, se integra una nueva capa denominada \textit{Semanticware} que sustenta todo el \textbf{conocimiento del modelo semántico} y los servicios, incluyendo los procesos necesarios para realizar la composición y reconfiguración de los servicios.

A continuación, se describirá de forma mas detallada cada una de las capas de la arquitectura propuesta.

\subsection{Servicios}
Un servicio es una unidad funcional discreta que realiza una tarea determinada y a la que se puede acceder de forma remota e independiente. En los ecosistemas IoT podemos encontrar distintos servicios desplegados, los cuales pueden ser clasificados en distintos niveles:

\begin{itemize}
	\item \textbf{Edge}: A este nivel se realiza un procesamiento de los datos muy cercano a la fuente que los genera. En la mayoría de los casos, los servicios son implementados en el dispositivo, por lo cual existe un fuerte acoplamiento entre el servicio y el dispositivo. 
	\item \textbf{Fog}: Este nivel proporciona una infraestructura de computación descentralizada en la que los datos, el cómputo, el almacenamiento y las aplicaciones están distribuidos en el lugar más lógico y eficiente entre la fuente de datos y la nube. Comúnmente se realizan tareas de agregación, manipulación y procesamiento de datos que no pueden ser realizadas en el nivel Edge, por medio de gateways de aplicaciones o hubs.
	
	\item \textbf{Cloud}: Este paradigma permite ofrecer servicios de computación a través de Internet. A este nivel se puede realizar un procesamiento de cantidades muy elevadas de datos, por lo cual la optimización de recursos y la eficiencia en costes son algunas de las ventajas mas relevantes de este modelo de computación.	
\end{itemize}

Las Arquitecturas Orientadas a Servicios (\ac{SOA}) tradicionales han considerado los servicios en el mismo nivel, pero con esta arquitectura no se pretende hacer esa distinción, simplemente se persigue un servicio de homogeneización que permita ver a los servicios de igual forma, independientemente del enfoque computacional que sigan (Edge, Fog o Cloud). El hecho de que todos los servicios puedan ser tratados por igual permite automatizar la tarea de composición y reconfiguración de los servicios de forma no supervisada.

Comúnmente las tecnologías de comunicación subyacentes a los servicios Edge están fuertemente acopladas. Esto se debe a que los dispositivos disponen de capacidades limitadas en cuanto a memoria y procesamiento, lo que conduce a la imposibilidad de  implementar la pila TCP/IP completa. Por lo cual, en la mayoría de los casos no es posible reemplazar los protocolos de propósitos específicos por uno estándar. 

IDM proporciona un mecanismo para interconectar los objetos para que puedan comunicarse entre sí de forma simétrica,  para el cual no es necesaria una transformación de los mensajes que se envían a través de la red ni la configuración de manejadores o enrutadores de mensajes.
Uno de los aspectos mas importantes de IDM es que cada uno de los recursos de la red IoT (cada sensor o actuador) es un objeto. Esto implica que si un solo dispositivo tiene mas de un recurso, un objeto individual será asignado para cada recurso.

\subsection{Middleware}
La arquitectura propuesta usa en la capa middleware, un middleware orientado a objetos de propósito general como es ZeroC Ice. Ice provee un conjunto de herramientas, APIs y librerías para soportar el desarrollo de aplicaciones orientadas a objetos del tipo cliente-servidor en entornos heterogéneos en los cuales las aplicaciones pueden ser desarrolladas con diferentes lenguajes de programación. 

Para ofrecer un tratamiento uniforme de los servicios desarrollados en Ice, el middleware dispone de un lenguaje de especificación de interfaces denominado \textit{Slice}. Cada objeto Ice, tiene una interfaz asociada con la que se define el  numero de operaciones que puede realizar y los tipos de datos que intercambia entre el cliente y el servidor. Por medio de las interfaces Slice, el desarrollador puede generar bindings para distintos lenguajes, de forma que la interoperación entre clientes y servidores está soportada de facto gracias al protocolo Ice (IceP\footnote{\href{https://doc.zeroc.com/display/Ice36/The+Ice+Protocol}{https://doc.zeroc.com/display/Ice36/The+Ice+Protocol}}).

ZeroC Ice dispone de un conjunto de herramientas y servicios que permiten tratar problemas recurrentes en sistemas distribuidos como el soporte para la propagación de datos, el despliegue o la gestión de nodos y plataformas. Algunos de los servicios de los que dispone son los siguientes\footnote{\url{https://doc.zeroc.com/display/Ice36/Ice+Services+Overview}}: Freeze, IceGrid, IceBox Server, IceStorm, IcePatch2 y Glacier2. En la arquitectura propuesta, se propone extender las capacidades de dicho middleware a través del diseño de distintos servicios que facilitan las tareas involucradas en la composición de servicios:

\begin{itemize}
	\item \textbf{Servidor de propiedades}: Este servicio pretende sustentar la información estática de los diferentes elementos que comprenden al sistema como servicios, dispositivos, localizaciones, etc. Ese servicio es implementado como una base de datos clave-valor, en la cual cada propiedad (o clave) tiene asociada un valor.
	\item \textbf{Servicio de descubrimiento}: Este servicio permite obtener una lista de servicios que cumplen con unos determinados requerimientos en términos de localización, capacidades o propiedades. 
	\item \textbf{Servicio de contexto}: Este servicio se comporta como un directorio de servicios y dispositivos que están organizados jerárquicamente, de forma que los servicios que provean una misma funcionalidad, o que estén localizados en una determinada localización, queden dispuestos bajo el mismo directorio.
	\item \textbf{Servicio de eventos avanzado}: Este servicio soporta el filtrado de eventos y tiene capacidades de persistencia para poder seleccionar que eventos relacionados con un determinado patrón sean filtrados mediante una determinada regla.
\end{itemize}

\subsection{Agentes BDI}
Los estados mentales o cualidades mentales referidas por Mccarthy \cite{Mcc79} trata sobre cómo representar la información sobre creencias, conocimiento, libre albedrío, intenciones, conciencia, capacidad o deseos, que representan aspectos esenciales de la racionalidad humana. El trabajo de Bratman \cite{Bratman1987-BRAIPA} propone el modelo BDI (\textit{Belief, Desire and Intentions}) para el razonamiento práctico, como una explicación de la racionalidad humana que exhibe comportamientos dirigidos por objetivos.

Las metas insatisfechas es lo que motiva a las personas a diseñar planes que conduzcan a la satisfacción, el logro o el mantenimiento de metas, y por lo tanto, la emulación del comportamiento inteligente debe vincularse inexorablemente a una representación adecuada de los eventos mentales involucrados en emular comportamientos guiados por objetivos.

Además, el conocimiento del contexto es uno de los principales requisitos para habilitar espacios inteligentes, ya que la única forma de reaccionar sabia y proactivamente a los eventos contextuales, es comprender qué está sucediendo en el entorno. Los eventos o acciones que tienen lugar en el contexto se capturan a través de los dispositivos y servicios de detección desplegados en el entorno. Por lo tanto, el único rastro que evidencia la ocurrencia de un evento son los valores de detección capturados por cualquiera de estos objetos IoT. Asignar esos valores a los efectos de un evento o una acción es la única forma posible de interpretación.

La forma de reducir la distancia entre la situación actual y una específica es mediante la elaboración de un plan, entendido aquí como un curso de acciones. La planificación de acciones tiene como objetivo aplicar cambios consecutivos a un estado inicial para transformarlo en el estado objetivo.

En la arquitectura propuesta, a este nivel se propone un conjunto de agentes software que se encargarán de supervisar los eventos para detectar objetivos insatisfechos. Como resultado de dicha detección, se lanzarán planes para restaurar el estado deseado. Los agentes impulsados por objetivos se han construido sobre las capas semántica y de middleware, lo que significa que los aspectos de comunicación son totalmente transparentes para los agentes, mientras que el conocimiento está disponible en la base de conocimiento de Scone.

Estos agentes de BDI entienden \textit{<<beliefs>>} como las propiedades que un agente considera verdaderas, \textit{<<goals>>} como las propiedades que un agente desea que sean ciertas y, finalmente, \textit{<<plans>>} como las acciones que llevan a un agente a un objetivo deseado. Estas instancias básicas definen lo que se conoce como el estado mental del agente.

Las creencias del agente en combinación con la información contextual (contenida en la base de conocimiento Scone) son las que conducen el comportamiento del agente hacia los objetivos que el agente desea alcanzar o mantener. La interacción entre los agentes, la base de conocimiento, los servicios y los dispositivos se basa en el hecho de que todos comparten el mismo modelo semántico.

Considerando el escenario planteado en este proyecto, la situación en la que una persona se encuentra frente del despacho, buscando su llave o tarjeta de acceso, dándose cuenta de que la ha olvidado en su casa. El sistema debe idear una forma de otorgar acceso a esta persona, sabiendo que él/ella está autorizado para acceder a ese espacio. El agente que supervisa el contexto mantiene el siguiente estado mental:

\begin{itemize}
	\item \textit{\textbf{Belief (a, b)}}: el agente \textit{a} cree que ha tenido lugar un evento de intento de acceso autorizado \textit{b}.
	\item \textit{\textbf{Goal (a, g)}}: el agente desea alcanzar la meta \textit{g} que otorga acceso a esa persona.
	\item \textbf{\textit{Plan (a, p)}}; El agente \textit{a} recurre a un conjunto de acciones o plan \textit{p} para permitir que la persona acceda al despacho.
\end{itemize}

En la arquitectura propuesta, esta capa permitiría inferir los dos primeros procesos: la detección de la necesidad y la inferencia evento o meta que ha de ser satisfecha. Posteriormente, dicho evento es el que guiará la ejecución del planificador en la composición de los servicios.

\subsection{Semanticware}

El termino \textit{semanticware} ha sido ideado para hacer referencia a la capa que sustenta la información semántica del sistema. El principal propósito de esta capa, es ofrecer un nivel de abstracción que de soporte al intercambio de información, garantizando la consistencia semántica de los servicios. De forma análoga a las interfaces de programación definidas en el middleware de comunicaciones, a este nivel se define un modelo semántico que captura los conceptos y relaciones que permiten una definición semántica consistente del sistema. 

El lenguaje de descripción de servicios ideado durante el desarrollo del proyecto, ha permitido establecer un mecanismo con el que definir de forma exacta las funcionalidades provistas por un servicio y cómo evoluciona el entorno tras su ejecución. Esto ha permitido capturar la semántica del sistema en la base de conocimiento Scone, para poder realizar procesos de inferencia a través del planificador.

En el planteamiento actual, el planificador no tiene en cuenta la distribución espacio temporal de los servicios y los dispositivos, por lo cual la composición de servicios está basada únicamente en la funcionalidad provista por los servicios. En una evolución natural del middleware, dichos aspectos han de ser contemplados bajo esta capa para poder realizar una reconfiguración y composición de los servicios adecuada.

\section{Sistema de control de acceso inteligente}
El sistema de control de acceso inteligente desarrollado a lo largo de este proyecto ha sido instalado en la puerta y el pasillo colindante al despacho múltiple del grupo de investigación ARCO localizado en la primera planta del Instituto de Tecnologías y Sistemas de Información, tal y como se ilustró en la Figura \ref{fig:plano-laboratorio}. 

\begin{figure}[h]
	\begin{center}
		\includegraphics[width=0.80\textwidth]{figures/despliegue-camara.jpg}
		\caption{Instalación de la cámara de vigilancia y el micrófono }
		\label{fig:despliegue-camara}
	\end{center}
\end{figure}

En la Figura \ref{fig:despliegue-camara} se puede observar como ha quedado la instalación de la cámara de vigilancia y el micrófono en el marco exterior de la puerta del despacho.

Con la finalidad de ocultar en la medida de lo posible la instalación de los dispositivos, se ha canalizado el cableado de la cámara de vigilancia y el micrófono hasta una regleta instalada en el pasillo colindante al despacho, donde se ha introducido el hardware correspondiente para alimentar los dispositivos y la Raspberry Pi (ver Figura \ref{fig:despliegue-rpi}).

\begin{figure}[h]
	\begin{center}
		\includegraphics[width=0.80\textwidth]{figures/despliegue-rpi.jpg}
		\caption{Instalación de la Raspberry Pi y el adaptador de corriente de la cámara}
		\label{fig:despliegue-rpi}
	\end{center}
\end{figure}

En el interior del despacho se realizó la instalación de los dispositivos encargados del accionamiento del abrepuertas eléctrico, tales como el Sonoff y el driver de led (ver Figura \ref{fig:despliegue-sonoff}). 

\begin{figure}[h]
	\begin{center}
		\includegraphics[width=0.80\textwidth]{figures/despliegue-sonoff.jpg}
		\caption{Instalación de los controladores de la cerradura electromecánica}
		\label{fig:despliegue-sonoff}
	\end{center}
\end{figure}

El diagrama de conexiones de los dispositivos del control de acceso queda ilustrado en la Figura \ref{fig:diagrama-conexiones}. En el Anexo \ref{anexo:2} se ha detallado todo el proceso de configuración de los distintos dispositivos que conforman el sistema. 

\begin{figure}[h]
	\begin{center}
		\includegraphics[width=0.80\textwidth]{figures/conexion-logica-dispositivos.png}
		\caption{Diagrama de conexiones de los dispositivos}
		\label{fig:diagrama-conexiones}
	\end{center}
\end{figure}

Con lo que respecta a los servicios, se han desarrollado dos aplicaciones: una con los prototipos de los servicios que simulan el comportamiento real de los mismos, y otra con los servicios funcionales que emplean los dispositivos aquí descritos. Para la correcta ejecución de la aplicación funcional, es necesario disponer de los dispositivos y configurar los distintos parámetros de los servicios. Todo el proceso para llevar a cabo dicha tarea está descrito en el Anexo \ref{anexo:1}.

Las aplicaciones han sido desarrolladas utilizando el servicio IceGrid del middleware de comunicaciones ZeroC Ice. IceGrid proporciona una gran variedad de funcionalidades para gestión remota de la aplicación distribuida, activación automática (implícita) de objetos, balanceo de carga y transparencia de localización, entre otros. 
Los distintos servidores que componen la aplicación del sistema de control de acceso son los siguientes:

\begin{itemize}
	\item \textbf{Motion sensor}: Servicio encargado de generar un evento de tipo EventSink, cuando se detecta la creación de un archivo \textit{.jpg} en un directorio FTP de la Raspberry Pi. 
	\item \textbf{Snapshot service}: Servicio encargado de realizar una captura de imagen a través de la cámara de vigilancia, realizando una petición HTTP a la misma.
	\item \textbf{Clip service}: Servicio encargado de la grabación de audio utilizando el micrófono conectado a la Raspberry Pi.
	\item \textbf{Person recognizer}: Servicio encargado de realizar el reconocimiento facial sobre una imagen, para extraer la identificación de una persona utilizando la librería Openface.
	\item \textbf{Speech to text}: Servicio encargado de realizar la conversión de voz a texto utilizando el API de IBM Watson.
	\item \textbf{Authenticator}: Servicio encargado de autorizar la ejecución de un comando.
	\item \textbf{Door actuator}: Servicio encargado del control del abrepuertas eléctrico, implementado sobre el Sonoff.
	\item \textbf{Scone wrapper}: Servidor encargado de cargar los archivos .lisp del directorio scone-knowledge.d, que contienen toda la semántica referente al sistema.
	\item \textbf{Icebox}: Servidor de aplicaciones que permite lanzar el servicio Icebox, utilizado por el Wiring service, para la gestión de canales de eventos.
	\item \textbf{Property server}: Servidor auxiliar utilizado por el servicio Wiring service para almacenar la información referente a las conexiones realizadas entre los servicios.
	\item \textbf{Wiring service}: Servicio encargado de automatizar la tarea de cableado de servicios. 
\end{itemize}

Todos los servicios aquí descritos se ejecutarán en la Raspberry Pi, a excepción del door actuator que es un objeto Ice que se ejecuta en el chip ESP8266 del Sonoff. El proceso de flasheo del firmware está descrito en la Anexo \ref{anexo:2}.

\section{Presupuesto del proyecto}
El desarrollo del proyecto se llevó a cabo durante la estancia como contratado en prácticas e investigador en el grupo de investigación ARCO. El periodo de prácticas fue vigente desde febrero de 2017 hasta junio del mismo año, cuya dedicación fue de 75 horas/mes.

A partir de julio de 2017 hasta enero de 2018 se estableció un contrato como investigador por el cual se estipulaba una dedicación de 150 horas/mes los primeros 4 meses y de 75 horas/mes los cuatro últimos. Teniendo en cuenta las horas de trabajo realizadas, se ha hecho una estimación de las horas empleadas para cada fase del proyecto (ver Cuadro \ref{estimacion-horas}). 

\begin{table}[]
	\centering

	\begin{tabular}{|lr|l}
		\cline{1-2}
		\cellcolor[HTML]{C0C0C0}\textbf{Fase}  & \cellcolor[HTML]{C0C0C0}\textbf{Horas empladas (aprox.)} & \multicolumn{1}{c}{\textbf{}} \\ \cline{1-2}
		Analisis de requisitos                 & 40 horas                                                 &                               \\ \cline{1-2}
		Diseño del sistema                     & 240 horas                                                &                               \\ \cline{1-2}
		Implementación						   & 650 horas                                                &                               \\ \cline{1-2}
		Instalación dispositivos               & 20 horas                                                 &                               \\ \cline{1-2}
		Redacción del TFG                      & 250 horas                                                &                               \\ \cline{1-2}
		\cellcolor[HTML]{C0C0C0}\textbf{Total} & \cellcolor[HTML]{C0C0C0}\textbf{1200 horas}              &                               \\ \cline{1-2}
	\end{tabular}
	\caption{Estimación de horas empleadas para cada fase del proyecto}
	\label{estimacion-horas}
\end{table}

En base a la estimación de horas, se ha presupuestado el proyecto y los gastos realizados a lo largo de su desarrollo en el Cuadro \ref{costes-proyecto}.

\begin{table}[]
	\centering

	\begin{tabular}{|lr|}
		\hline
		\rowcolor[HTML]{C0C0C0} 
		\textbf{Concepto}           & \textbf{Coste}  \\ \hline
		\rowcolor[HTML]{FFFFFF} 
		August Smart Keypad         & 79\euro                 \\ \hline
		\rowcolor[HTML]{FFFFFF} 
		August Smart Lock Pro (v.2) & 229\euro                \\ \hline
		\rowcolor[HTML]{FFFFFF} 
		August Doorbell Cam (v.1)   & 199\euro                \\ \hline
		\rowcolor[HTML]{FFFFFF} 
		Camara IP Foscam C1         & 69,99\euro              \\ \hline
		\rowcolor[HTML]{FFFFFF} 
		Raspberry Pi 2              & 33,74\euro              \\ \hline
		\rowcolor[HTML]{FFFFFF} 
		Microfono de solapa         & 8,99\euro               \\ \hline
		\rowcolor[HTML]{FFFFFF} 
		Logilink USB Soundcard      & 10,39\euro              \\ \hline
		\rowcolor[HTML]{FFFFFF} 
		Sonoff Basic                & 4,59\euro               \\ \hline
		\rowcolor[HTML]{FFFFFF} 
		Driver de LED               & 2,65\euro               \\ \hline
		\rowcolor[HTML]{FFFFFF} 
		Abrepuertas Dorcas          & 25,93\euro              \\ \hline
		\rowcolor[HTML]{FFFFFF} 
		Personal                    & 1200 horas x 40\euro/hora    \\ \hline
		\rowcolor[HTML]{C0C0C0} 
		\textbf{Total}              & \textbf{36663,28 \euro} \\ \hline
	\end{tabular}
	\caption{Costes del proyecto}
	\label{costes-proyecto}
\end{table}

\section{Publicaciones}
Como resultado del planteamiento de este proyecto, se ha presentado un articulo denominado <<\textit{\textbf{Automatic Service Composition for IoT-based in Smart Homes}}>> que se encuentra bajo revisión para ser publicado en la revista \textit{International Journal of Distributed Sensor Networks (IJDSN)}\footnote{\url{http://journals.sagepub.com/home/dsn}} en su edición especial \textit{Sensor-based Smart Home Technologies and Applications} el día 30 de Diciembre del 2017.

\section{Código fuente}
\label{sec:cerberus}
Todo el código fuente generado para el desarrollo del presente proyecto se encuentra almacenado en el siguiente repositorio público: \url{https://bitbucket.org/arco_group/cerberus}. El repositorio se ha estructurado de tal forma que su manejo e interpretación sea lo mas sencilla posible:

\begin{itemize}
	\item \textbf{services}: En este directorio se encuentran todos los servicios desarrollados a lo largo del proyecto, tanto los prototipados como los funcionales. La distinción entre los mismos es observable porque los servicios prototipados tienen la palabra \textit{dummy} como prefijo.
	\item \textbf{config}: Este directorio contiene las aplicaciones desarrolladas en formato \textit{.xml} que han de ser cargadas por el servicio Icegrid para la ejecución de los servicios. Además, se dispone de archivos de configuración para la ejecución de un nodo Icegrid en localhost.
	\item \textbf{devices}: Este directorio contiene el código fuente que se debe flashear en el Sonoff para el control del abrepuertas.
	\item \textbf{scone-knowledge.d}: Este directorio contiene todo el código \textit{.lisp} referente a la semántica del sistema. Cada uno de los archivos \textit{.lisp} serán cargados por el servidor scone-wrapper.
	\item \textbf{tests}: Este directorio contiene algunos pruebas unitarias para comprobar el funcionamiento del código del planificador y la base de conocimiento Scone.
	\item \textbf{install-dependencies.sh}: Este script permite automatizar la instalación de las dependencias de los servicios, para el correcto funcionamiento de los mismos en sistemas Debian o Ubuntu.
\end{itemize}

Por otra parte, toda la documentación generada durante el proyecto se encuentra almacenada en el repositorio público: \url{https://bitbucket.org/arco_group/tfg.cristian.trapero/}. 

