Descripción
===================
Este repositorio engloba todo el contenido del Trabajo Fin de Grado «**Middleware de comunicaciones para la composición automática de servicios en IoT**». El proyecto define una arquitectura con la que habilitar la composición automática de servicio, a través de un middleware con capacidades semánticas. Dicho middleware permitirá abstraer los detalles de implementación subyacente a cada uno de los servicios que se integran en un ecosistema IoT.

----------
- **Autor**: Cristian Trapero Mora
- **Director**: Maria Jose Santofimia
- **Director**: David Villa Alises

----------
Estructura del repositorio
-------------

 - **doc**: En este directorio se encuentran todos los archivos referentes a la documentación generada durante el proyecto, tales como el anteproyecto o la memoria del mismo.

El código fuente del proyecto está en el repositorio: [https://bitbucket.org/arco_group/cerberus](https://bitbucket.org/arco_group/cerberus)
